---
title: "Glass eel migration and recruitment within a complex Mediterranean hydro-system"
layout: splash
author_profile: true
classes:
    - landing
    - dark-theme
permalink: /research_GlassEelCamargue/
header:
  overlay_color: "#000"
  overlay_filter: "0.1"
  overlay_image: photos_images/P_Photo_Vaccares.jpeg
  caption: "©C. Bouchard"
excerpt: "   "
last_modified_at: 2022-10-04
---

---
<figure style="width: 400px" class="align-left">
  <img src="{{ site.baseurl }}/photos_images/P_Plot_S_WGEEL_Plot_WGEEL_Estimates_GEel_WGEEL_200820_ColorInvert.pdf" alt="">
  <figcaption> The WGEEL recruitment index. ©Colin Bouchard.</figcaption>
</figure> 


<p align="justify">
The European eel (<i>Anguilla anguilla</i>) colonizes European coastal waters and rivers to growth. Like many other migratory fishes, the European eel recruitment declines since the 1980s (see Figure on the left, ICES 2020). Climate change, habitat loss, and fragmentation (presence of dams and other barriers), or fishing and trafficking represent the main threats to European eels (Drouineau et al. 2018). European states are forced to implement and enforce management policies that allow the recovery of escapement levels (i.e., silver eels migrating to the sea) observed before the decline. Part of the need for these policies is to monitor glass eel recruitment (i.e., the abundance of glass eels migrating to fresh or brackish waters), to better understand the migratory behavior of glass eels in coastal hydro-systems, and to identify actions to improve both recruitment and migration of glass eels, as well as, to mitigate pressures. 
</p>


<figure style="width: 400px" class="align-right">
  <img src="{{ site.baseurl }}/photos_images/S03_SIG_Carto_StudyArea_A01_DeltaEntryPathways_AllStation.pdf" alt="">
  <figcaption> Map of the Rhone delta. ©Colin Bouchard.</figcaption>
</figure> 


<p align="justify">
Most studies focused on glass eel recruitment are on the Atlantic coasts. However, Mediterranean coastlines offer multiple, highly diverse, and potentially highly productive habitats for glass eels and eels (i.e., small or long rivers, lagoons, marshes, ...). In the Camargue, such habitats are available and accessible for glass eels through several migration pathways (see the map). The first aim of this project was to identify and caracterise the temporal dynamics of the recruitment for the entire hydro-system, but also to identify spatial heterogeneity within the delta. We used several time-series of glass eel recruitment to build a hierarchical state-space Bayesian model. The philosophy behind this model was to estimate latent unobserved recruitment for the entire hydro-system, latent recruitment distributed among months and sites. Our model enabled us to estimate inter-annual and intra-annual variations in recruitment while accounting for spatial heterogeneity within the delta. Our results provide knowledge for glass eel recruitment along the French Mediterranean coasts: inter-annual dynamics and an estimation of the migration peak for this system, which can be compared to other Mediterranean systems.  
</p>

<figure style="width: 400px" class="align-left">
  <img src="{{ site.baseurl }}/photos_images/P_Photo_Fourcade.jpeg" alt="">
  <figcaption> The Fourcade barrier constituted by 13 sluice gates. ©Colin Bouchard.</figcaption>
</figure> 

<p align="justify">
Furthermore, a barrier is located on the main migration way (see the Figure of the Fourcade barrier). The second aim was to identify how environmental conditions and barrier management affect recruitment through three components: presence/absence, magnitude, and composition (glass eel pigmentary stages). Using different hierarchical Bayesian models, we have identified which environmental conditions are associated with higher recruitment levels and leading to the migration of young pigmentary stages, which are the most likely to colonize the system (Bureau Du Colombier et al. 2007, De Casamajor et al. 2003). Our results are in line with recent studies on barrier management effects on fish migration (Mouton et al. 2012, Van Wichelen et al. 2021) but go further than many studies on the effect of environmental co-variables by working on several seasons (Lagarde et al. 2021, Lanzoni et al. 2022, Podda et al. 2020, Van Wichelen et al. 2021), and three components of recruitment. 
</p>

<figure style="width: 400px" class="align-right">
  <img src="{{ site.baseurl }}/photos_images/Group_TampanMoi_ComtesseDNS.png" alt="">
  <figcaption> Two different monitoring sites within the Camargue. ©D Nicolas-Tour du Valat.</figcaption>
</figure> 

<p align="justify">
In this project, we also used glass eel monitoring data for several years and at several sites to assess the biological continuity within restored former salt marshes. Using two time-series located at the main entrance of the hydro-system and its center, we also estimated the migration speed of glass eels. This analysis is the first step to better understanding the upstream migration of glass eels in such specific hydro-systems. 
</p>

------------------------------------------------------------------------
More information and results here: 
-   **Bouchard C.**, Drouineau H., Lambert P., Boutron O., and D. Nicolas. **2022**. Spatio-temporal variations in glass eel recruitment at the entrance pathways of a Mediterranean delta. *ICES journal*, **79** (6): 1874-1887. [link](https://academic.oup.com/icesjms/article/79/6/1874/6633760)
-   **Bouchard C.**, Boutron O., and D. Nicolas. **2022**. Synthèse et analyse de la continuité écologique à l'échelle du delta de Camargue : caractérisation du recrutement et de la dynamique migratoire des civelles d'anguilles Européennes dans le delta de Camargue. *Rapport final action MA 12 - contrat de Delta Camargue - Phase 2* : 156. [link](https://colinbouchard.gitlab.io/personal-website/documents/Bouchardetal_RapportFinal_AERM.pdf)

------------------------------------------------------------------------

<figure style="width: 400px" class="align-center">

<img src="{{ site.baseurl }}/photos_images/P_Photo_EMSC_TamGal.jpg"/>

<figcaption>Fyke nets use to catch glass eels in the lagoon. ©Colin Bouchard.</figcaption>

</figure>


------------------------------------------------------------------------
Literature cited:
-   ICES. **2020**. Joint EIFAAC/ICES/GFCM Working Group on Eels Publisher: ICES. 
-   Drouineau, H., Carter, C., Rambonilaza, M., Beaufaron, G., Bouleau, G., Gassiat, A., Lambert, P., le Floch, S., Tétard, S. & de Oliveira, E. **2018**. River Continuity Restoration and Diadromous Fishes: Much More than an Ecological Issue. *Environmental Management*, **61**: 671-686. 
-   Bureau Du Colombier, S., Bolliet, V., Lambert, P. & Bardonnet, A. **2007**. Energy and migratory behavior in glass eels (<i>Anguilla anguilla</i>). *Physiology & Behavior*, **92**: 684–690. 
-   De Casamajor, M.N., Lecomte-Finiger, R. & Prouzet, P. **2003**. Caractéristiques biologiques des civelles (<i>Anguilla anguilla</i>) lors de la transition en estuaire. *Bulletin Français de la Pêche et de la Pisciculture*: 109-124. 
-   Lagarde, R., Peyre, J., Amilhat, E., Bourrin, F., Prellwitz, F., Simon, G. & Faliex, E. **2021**. Movements of Non-Migrant European Eels in an Urbanised Channel Linking a Mediterranean Lagoon to the Sea. *Water*, **13**. 
-   Lanzoni, M., Gavioli, A., Castaldelli, G., Aschonitis, V. & Milardi, M. **2022**. Swoon over the moon: The influence of environmental factors on glass eels entering Mediterranean coastal lagoons. *Estuarine, Coastal and Shelf Science*, **264**. 
-   Mouton, A.M., Huysecom, S., Buysse, D., Stevens, M., Van den Neucker, T. & Coeck, J.  **2012**. Optimisation of adjusted barrier management to improve glass eel migration at an estuarine barrier. *Journal of Coastal Conservation *. 
-   Podda, C., Palmas, F., Frau, G., Chessa, G., Culurgioni, J., Diciotti, R., Fois, N. & Sabatini, A. **2020**. Environmental influences on the recruitment dynamics of juvenile European eels, <i>Anguilla anguilla</i>, in a small estuary of the Tyrrhenian Sea, Sardinia, Italy. *Aquatic Conserva- tion: Marine and Freshwater Ecosystems*, **30** (6): 1638–1648.
-   Van Wichelen, J., Verhelst, P., Buysse, D., Belpaire, C., Vlietinck, K. & Coeck, J. **2021**. Glass eel (<i>Anguilla anguilla</i> L.) behaviour after artificial intake by adjusted tidal barrage management.  *Estuarine, Coastal and Shelf Science*, **249** (6): 107-127. 

------------------------------------------------------------------------