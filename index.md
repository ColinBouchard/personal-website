---
layout: home
author_profile: true
header:
    overlay_image: photos_images/IMG_9857.jpg
    caption: "Photo credit: ©Colin Bouchard"
title: 'Home'
image:
    feature: photos-images/IMG_9857.jpg
    credit: Colin Bouchard
permalink: /
last_modified_at: 2023-09-14
excerpt: "   "
---

---

Current position: Scientific project manager at [SCIMABIO Interface](https://www.scimabio-interface.fr) 

---

<p align="justify">
I am a behavioural ecologist interested in how BEHAVIOR and SPACE USE (i.e., movements and spatial distribution) may impact POPULATION DYNAMICS and SEXUAL SELECTION. At an individual level, I am interested in individual variability in behaviors associated with space use (e.g., migration phenotype, movements, distribution) and how it drives the social environment (e.g., local density, sexual environment), as well as spatial distribution. Additionally, I am interested in how environmental conditions (abiotic or biotic) may influence these behaviors. As a behavioral ecologist, I'm intrigued by the ultimate effects of those behaviors and the associated individual variability in survival and sexual selection. At a population level, my focus is on how individuals' spatial distribution and behavior may affect population dynamics through density-dependent processes and environmental effects.
</p>

---

[View my research projects]({{site.baseurl}}/research/){: .btn .btn--success}

------------------------------------------------------------------------
<figure style="width: 400px" class="align-center">

<img src="{{ site.baseurl }}/documents/Scimabio final-01.png"/>

</figure>
------------------------------------------------------------------------
