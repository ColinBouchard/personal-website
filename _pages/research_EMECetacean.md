---
title: "Investigate extreme mortality events in small cetaceans"
layout: splash
author_profile: true
classes:
    - landing
    - dark-theme
permalink: /research_EMECetacean/
header:
  overlay_color: "#000"
  overlay_filter: "0.1"
  overlay_image: photos_images/IMG_4887.jpg
  caption: "©Colin Bouchard"
excerpt: "A risk-based indicator for marine mammal conservation: strandings and probabilistic forecast of extreme mortality events"
last_modified_at: 2019-10-21
---

---
<figure style="width: 500px" class="align-left">
  <img src="{{ site.baseurl }}/photos_images/CommonDolphin_HPeltier.jpg" alt="">
  <figcaption>A dead common dolphin, ©H Peltier/ UMR PELAGIS.</figcaption>
</figure> 

<p align="justify">
Spatial distribution of cetaceans may lead to strong interactions between human activities (e.g., fisheries, maritime transport, ...) and cetaceans. For small cetaceans, bycatch by fisheries constitutes a significant threat (Dolman, Baulch, Evans, Read, & Ritter, 2016; Read, Drinker, & Northridge, 2006; Reeves, McClellan, & Werner, 2013) as bycatch causes additional mortality, which may have a strong impact on population dynamics (Taylor et al., 2017). Extreme Mortality Events (EMEs) correspond to short events of high levels of mortality. The European Marine Strategy Framework Directive (MSFD, 2008/56/EC) aims to maintain or restore a " Good Environmental Status" by using several indicators. We built and proposed a risk-indicator of EMEs for three species of small cetaceans.
</p>



<figure style="width: 500px" class="align-right">
  <img src="{{ site.baseurl }}/documents/Figure 5_BouchardEtAl2019.pdf" alt="">
  <figcaption>Corrected return levels (number of stranded individuals over three days) forecast from our model for harbor porpoises, common dolphins, and striped dolphins between January 2013 and December 2018 (MSFD cycle). The curve corresponds to the mean forecast and the gray area to the 95% HPD interval. The associated risk is color-coded for each estimated return level. ©From Bouchard et al, 2019.</figcaption>
</figure> 



<p align="justify">
Mortalities in cetaceans are usually monitored through beach strandings. We used strandings data and the Extreme Value Theory to build a model enabling policymakers to forecast EMEs as it is done for floods. Our model estimates a risk probability that a specific EME occurs under x years. By comparing observed EMEs and those predicted by our model, policymakers may assess how pressures leading to EMES of small cetaceans vary over time.  
</p>

<p align="justify">
This work was published in <i>Conservation Letters</i>, as well as the code for models. Data are available on <i>Dryad</i> (<a href="https://doi.org/10.5061/dryad.vj7sh73">link</a>). Furthermore, the French government uses this works as an <span style="font-weight:bold">indicator</span> in the MSFD (<a href="http://www.dirm.sud-atlantique.developpement-durable.gouv.fr/IMG/pdf/03-_evaluation_de_l_atteinte_du_bee_des_mammiferes_marins_au_titre_du_descripteur_1_p_241_a_258-fev2019.pdf">link</a>).
</p>
---

**Bouchard C.**, Bracken C.,  Dabin W., Van Canneyt O., Ridoux V., Spitz J., and M. Authier. **2019**. A risk-based indicator for marine mammal conservation: strandings and probabilistic forecast of extreme mortality events. *Conservation Letters*, **12** (4): 10. [link](https://doi.org/10.1111/conl.12639)

---
