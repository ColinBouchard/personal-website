---
title: "Spatial aggregation effects on population dynamics"
layout: splash
author_profile: true
classes:
    - landing
    - dark-theme
permalink: /research_SRAggreg/
header:
  overlay_color: "#000"
  overlay_filter: "0.1"
  overlay_image: documents/Fig5_P_Plot_Grid_RecruitmentStockBest_50m_RecruitmentPtau.jpg
  caption: "©Colin Bouchard"
excerpt: "   "
last_modified_at: 2019-10-21
---

---
<figure style="width: 500px" class="align-left">
  <img src="{{ site.baseurl }}/documents/SchemeAggregationSR.jpg" alt="">
  <figcaption> Scheme of the potential effects of spatial aggregation on recruitment. ©Colin Bouchard.</figcaption>
</figure> 

<p align="justify">
In a wide range of taxa, population dynamics exhibit yearly variations. Temporal variations in stock (e.g., number of eggs), <span style="font-weight:bold">density-dependent processes</span> (e.g., density-dependent compensation), or <span style="font-weight:bold">density-independent processess</span> (e.g., environmental perturbations) may affect population dynamics. In species where eggs are buried or do not disperse, variability in spatial distribution of breeders generates variability in spatial aggregation of stock. Variability in <span style="font-weight:bold">spatial aggregation</span> leads to variability in density-dependent processess acting on stock, environmental conditions experiencing by stock, and in the effects of environmental stochasticity. We, then, assessed how spatial aggregation of breeders affects population dynamics through stock-recruitments models.
</p>

<figure style="width: 400px" class="align-left">
  <img src="{{ site.baseurl }}/documents/SRCurves_Example.jpg" alt="">
  <figcaption> Example of stock-recruitment curves from the four mainly used models. ©Colin Bouchard.</figcaption>
</figure> 
<figure style="width: 400px" class="align-right">
  <img src="{{ site.baseurl }}/documents/Fig5_P_Plot_Grid_RecruitmentStockBest_50m_RecruitmentPtau.jpg" alt="">
  <figcaption> Effects of spatial aggregation on stock-recruitment models. ©From Bouchard et al, 2018.</figcaption>
</figure> 

<p align="justify">
Using long time-series of stock (density in eggs) and recruitment (density in juveniles) for an Atlantic salmon population (<i>Salmo salar</i>), we implemented a measure of spatial aggregation of breeders in stock-recruitment models.
We found that spatial aggregation seemed to interplay with density-dependent processes (e.g. density-compensation) and density-independent ones (e.g. environmental perturbations) to stabilise the population recruitment. The latter impact depended on the level of aggregation, as well as, the location of aggregates.
</p>

---

**Bouchard C.**, Bardonnet A.,  Buoro M., Prévost E., and C. Tentelier. **2018**. Effects of spatial aggregation of nests on population recruitment: the case of a small population of Atlantic salmon. *Ecosphere*, **9** (4). [link](https://doi.org/10.1002/ecs2.2178)

**Bouchard, C.** 2018. In Atlantic salmon, space use of potential breeders stabilises population dynamics and shapes sexual selection (Doctoral dissertation, Pau). [link](https://www.researchgate.net/publication/331563783_In_Atlantic_salmon_space_use_of_potential_breeders_stabilises_population_dynamics_and_shapes_sexual_selection)

---

<figure style="width: 500px" class="align-center">
  <img src="{{ site.baseurl }}/photos_images/P_Photo_Redd_IMG_9967_Low.jpeg" alt="">
  <figcaption> A salmon redd where the dome is beginning to emerge after a low flow period. ©Colin Bouchard.</figcaption>
</figure> 
