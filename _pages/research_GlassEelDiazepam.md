---
title: "Behavioural alterations of glass eels' migratory behaviour by a psychiatric drug"
layout: splash
author_profile: true
classes:
    - landing
    - dark-theme
permalink: /research_GlassEelDiazepam/
header:
  overlay_color: "#000"
  overlay_filter: "0.1"
  overlay_image: photos_images/GlassEel_Contamination/P_Photo_Old_Civelle_IMG_3289_Crop.jpg
  caption: "©C. Bouchard"
excerpt: "   "
last_modified_at: 2022-10-23
---

---
<figure style="width: 400px" class="align-left">
  <img src="{{ site.baseurl }}/photos_images/P_Plot_S_WGEEL_Plot_WGEEL_Estimates_GEel_WGEEL_200820_ColorInvert.pdf" alt="">
  <figcaption> The WGEEL recruitment index. ©Colin Bouchard.</figcaption>
</figure> 

<figure style="width: 400px" class="align-right">
  <img src="{{ site.baseurl }}/photos_images/GlassEel_Contamination/GlassEel_Chronotron_LowQuality.gif" alt="">
  <figcaption> Tagged glass eels in our behavior monitoring tank. ©Colin Bouchard.</figcaption>
</figure> 


<p align="justify">
The European eel (<span style="font-weight:italic">Anguilla anguilla</span>) colonizes European coastal waters and rivers to growth. Like many other migratory fishes, the European eel recruitment declines since the 1980s (see Figure on the left, ICES 2020). Climate change, habitat loss, and fragmentation (presence of dams and other barriers), or fishing and trafficking represent the main threats to European eels (Drouineau et al. 2018). European states are forced to implement and enforce management policies that allow the recovery of escapement levels (i.e., silver eels migrating to the sea) observed before the decline. 
</p>


<figure style="width: 400px" class="align-right">
  <img src="{{ site.baseurl }}/photos_images/GlassEel_Contamination/Scheme_EstuaryHabitatsComtamination.pdf" alt="">
  <figcaption> Scheme of glass eel migration within estuaries. ©Colin Bouchard.</figcaption>
</figure> 


<p align="justify">
When arriving into coastal waters, glass eels start to pigment and migrate upstream rivers. Glass eels colonize a wide range of ecosystems among which salt-marshes, lagoons, estuaries, or habitats far upstream (Daverat et al, 2006). In estuaries, glass eels' upstream migration behavior follows the alternation of flooding and ebbing tides through a mechanism called SELECTIVE TIDAL STREAM TRANSPORT (STST). In concrete terms, individuals use tidal currents within estuaries to migrate upstream: individuals move upward in the water column during the flood tide and swim down to the bottom during the ebb tide (Cresci, 2020). This specific migratory behavior is dependent on several environmental factors, such as water temperature, water discharge, moon phase, and luminosity. Thus, this complex behavior relies on specific activity patterns, and the individuals' ability to react to their environment. </p>

<figure style="width: 400px" class="align-left">
  <img src="{{ site.baseurl }}/photos_images/P_Photo_RiverMouth_IMG_1469_Low.jpeg" alt="">
  <figcaption> The estuary of the French Nivelle river. ©Colin Bouchard.</figcaption>
</figure> 

<p align="justify">
Like many other migratory fishes, glass eels can pass through effluents from waste-water treatment plants when migrating through estuaries. Then, they have to face to and cope with a highly diversified and rising chemical pollution, among which pharmaceutical drugs. Pharmaceutical drugs have been increasingly consumed by humans and can be found in the outlet of waste-water treatment plants (Argaluza et al. 2021). Among the more studied pharmaceutical drugs, we found anxiolytic drugs such as benzodiazepines, oxazepam, and diazepam (Argaluza et al. 2021, Gebauer et al. 2011, Hellström et al. 2016, Huerta et al. 2016, Sundin et al. 2019). Previous studies have already highlighted numerous effects of anxiolytic drugs on fish behaviours (Brodin et al. 2014, Cooke et al. 2022, Jacquin et al. 2020). Previous studies have shown contamination with anxiolytic drugs was associated with an increase in boldness and activity (Brodin et al. 2017; 2014, Gebauer et al. 2011, Jacquin et al. 2020, Sundin et al. 2019) and a reduction in avoidance behaviour (Klaminder et al. 2019, Saaristo et al. 2017). In a migratory context, anxiolytic drugs favor the migration of salmon with contaminated individuals migrating faster than control individuals (Hellström et al. 2016). Behavioural alterations induced by anxiolytic drugs may also raise the probability of being predated in salmon smolts (Klaminder et al. 2019). This last example highlights the potential effects of changes in behavioural traits due to anxiolytic drugs on survival, and thus, evolution (Jacquin et al. 2020, Saaristo et al. 2018). 
</p>

<figure style="width: 400px" class="align-right">
  <img src="{{ site.baseurl }}/photos_images/GlassEel_Contamination/P_Photo_Tagging_IMG_4362.jpeg" alt="">
  <figcaption> Glass eels tagged with VIE-tag. ©C. Bouchard</figcaption>
</figure> 

<p align="justify">
In this project, we assessed the effects of diazepam, an anxiolytic drug, on the migratory behavior of glass eels. Precisely, we hypothesized that diazepam may reduce swimming activity of individuals, and alter their swimming behavior. We also tested whether diazepam reduce individual's stress by testing the effect of diazepam on the swimming activity resumption after individual manipulation. 
</p>

<figure style="background-color:grey;width: 600px" class="align-left">
  <img src="{{ site.baseurl }}/photos_images/GlassEel_Contamination/scheme_chronic_contamination.pdf" alt="">
  <figcaption> Scheme of the installation used to perform the chronic contamination of individuals. ©C. Bouchard</figcaption>
</figure> 

<p align="justify">
We used 80 individuals, among which 40 were contaminated with diazepam. The contamination was performed during 7 days, as a chronic contamination at an environmental concentration level. Thereafter, we placed the 80 individuals into an behavioral monitoring annular tank that mimic a river since individuals may continuously swim against or with the current.
</p>


<figure style="background-color:grey;width: 600px" class="align-left">
<img src="{{ site.baseurl }}/photos_images/GlassEel_Contamination/scheme_behaviour_tank_column.pdf"/>
<figcaption>Scheme of the behavior monitoring annular tank used. ©C. Bouchard.</figcaption>
</figure>


<p align="justify">
We found contaminated individuals had a faster recovery of swimming activity, and thus exit from shelters, than healthy individuals. Less contaminated individuals were active per sequence watch, this difference increased over time, despite the depuration. Also, we found contaminated individuals were less active than healthy individuals, and their swimming behavior was different.
</p>


------------------------------------------------------------------------
Most relevant literature cited:
-   Argaluza, J., Domingo-Echaburu, S., Orive, G., Medrano, J., Hernandez, R. et al. **2021**. Environmental pollution with psychiatric drugs. – *World Journal of Psychiatry* **11**(10): 791–804.
-   Brodin, T., Piovano, S., Fick, J., Klaminder, J., Heynen, M. et al. **2014**. Ecological effects of pharmaceuticals in aquatic systems—impacts through behavioural alterations. – *Philosophical Transactions of the Royal Society B: Biological Sciences* **369**(1656): 20130580.
-   Cresci, A. **2020**. A comprehensive hypothesis on the migration of European glass eels ( Anguilla anguilla ). – *Biological Reviews* **95**(5): 1273–1286.
-   Daverat, F., Limburg, K., Thibault, I., Shiao, J., Dodson, J. et al. **2006**. Phenotypic plasticity of habitat use by three temperate eel species, *Anguilla anguilla*, *A. japonica* and *A. rostrata*. – *Marine Ecology Progress Series* **308**: 231–241.
-   Drouineau, H., Carter, C., Rambonilaza, M., Beaufaron, G., Bouleau, G., Gassiat, A., Lambert, P., le Floch, S., Tétard, S. & de Oliveira, E. **2018**. River Continuity Restoration and Diadromous Fishes: Much More than an Ecological Issue. *Environmental Management*, **61**: 671-686. 
-   ICES. **2020**. Joint EIFAAC/ICES/GFCM Working Group on Eels Publisher: ICES. 
-   Jacquin, L., Petitjean, Q., Côte, J., Laffaille, P. and Jean, S. **2020**. Effects of Pollution on Fish Behavior, Personality, and Cognition: Some Research Perspectives. – *Frontiers in Ecology and Evolution* **8**: 86.
-   Klaminder, J., Jonsson, M., Leander, J., Fahlman, J., Brodin, T. et al. **2019**. Less anxious salmon smolt become easy prey during downstream migration. – *Science of The Total Environment* **687**: 488–493.


------------------------------------------------------------------------