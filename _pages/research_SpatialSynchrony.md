---
title: "Spatial synchrony between Atlantic salmon populations"
layout: splash
author_profile: true
classes:
    - landing
    - dark-theme
permalink: /research_SpatialSynchrony/
header:
  overlay_color: "#000"
  overlay_filter: "0.1"
  overlay_image: documents/Scheme_PEContext.jpg
  caption: "©Colin Bouchard"
excerpt: "   "
last_modified_at: 2022-10-04
---

---
<figure style="width: 400px" class="align-left">
  <img src="{{ site.baseurl }}/photos_images/M01_Map_StudyArea_Simple.jpg" alt="">
  <figcaption> Map of the Atlantic salmon metapopulation in Brittany (France) ©Colin Bouchard / Data from HydroFrance©</figcaption>
</figure> 


<p align="justify">
Understanding the drivers of ecological stability is mandatory to predict the response of organisms to environmental changes. The <span style="font-weight:bold">portfolio effect</span> (hereafter PE) describes how diversity between and within populations can increase <span style="font-weight:bold">temporal stability</span> of the complex of populations (Carlson & Satterthwaite 2011, Satterthwaite & Carlson 2015). For salmon, individuals in one stream can be viewed as assets (subpopulations) within watersheds (populations) within a portfolio (a set of interconnected populations, i.e. metapopulation). If populations react differently to environmental variability, then the metapopulation may experience reduced variability, including a reduced risk of collapse or decline. Populations reacting better than others may compensate losses in recruitment. Thus, a metapopulation portfolio is characterized by its stability and resilience to environmental perturbations which rely on the connectivity between these populations(Anderson et al. 2013, Braun et al. 2016) and the diversity of responses of populations to the environment (Elmqvist et al. 2003) generating <span style="font-weight:bold">asynchrony between populations</span>. Individuals adopting straying behaviour enable connectivity between populations. This spatial structure has important and often overlooked consequences in terms of genetic diversity (gene flow), demography, and management (Buoro & Carlson 2014, Carlson et al. 2014). Unfortunately, quantifying metapopulation portfolio is still challenging because of the lack of long-term datasets.
</p>


<figure style="width: 400px" class="align-right">
  <img src="{{ site.baseurl }}/documents/P_MatrixPlot_RhoEstimates_CorrMultivar_noTime_Nrecruit-1.jpg" alt="">
  <figcaption> Matrix of synchrony in juvenile abundance between 18 sub-populations of Atlantic salmon in France. ©Colin Bouchard.</figcaption>
</figure> 


<p align="justify">
 This project assesses the metapopulation functioning and PE by quantifying asynchrony between Atlantic salmon populations in France (Brittany). To do so, I use long-term time-series data of abundance and recruitment of Atlantic salmon populations provided by INRA ([ORE-DiaPFC](https://www6.inra.fr/diapfc)) and partners of the project (AFB, Anglers associations, Fisheries agencies, BGM,...). My project aims to model dynamics of all population into a unique and coherent framework using hierarchical Bayesian modelling and evaluate the asynchrony between them. Such analysis and similar work have been already performed by [S. Carlson](https://nature.berkeley.edu/carlsonlab/) on the metapopulation of Chinook salmon in the Central valley (United. States). This work enabled to identify which basin and population rivers the management effort should target. I will also investigate the sources of asynchony and PE by comparing adult and juvenile phases while accounting for environmental effects. Asynchrony and PE are usually assessed by focusing on the number of anadromous adults returning into rivers. Focusing on adult stage informs on the stock of spawners and provides information about roamers and strayers which are the individuals fuelling flows between populations. However, the juvenile stage as a source of asynchrony and diversity in populations is poorly appreciated. By comparing the asynchrony and PE at the adult stage and the juvenile stage will enable to identify the stage and populations which need to be the primary targets of specific management actions. In addition, this new approach will permit to investigate the potential effects of the environment (e.g. water temperature and flow, sea growth conditions) on synchrony of population at the adult and juvenile phases. Investigating the relative contribution of these mechanisms over a meta-population dynamic system is vital to better understand the ecological management approaches that can promote stabilizing PEs (Anderson et al. 2013) and resilience of natural resources.
</p>

This project is funded with a grant from the consortium E2S ([<i class="fas fa-globe"></i>](https://e2s-uppa.eu/en/index.html)).


---
  - **Bouchard C.**, Buoro M., Lebot C., and S. M. Carlson. **2021**.  Synchrony in population dynamics of juvenile Atlantic salmon: analyzing spatio-temporal variation and the influence of river flow and demography. *Canadian Journal of Fisheries and Aquatic Sciences*, **79** (5): 782-794. [link](https://doi.org/10.1139/cjfas-2021-0017)

---

<!--
---

---
-->

<figure style="width: 800px" class="align-center">
  <img src="{{ site.baseurl }}/photos_images/IMG_1079.jpg" alt="">
  <figcaption> River mouth of the Selune river (France) ©Colin Bouchard.</figcaption>
</figure> 
