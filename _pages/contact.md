---
title: "Contact"
layout: single
author_profile: true
classes:
    - landing
    - dark-theme
permalink: /contact/
header:
  overlay_color: "#000"
  overlay_filter: "0.1"
  overlay_image: photos_images/IMG_2251.jpg
  caption: "©Colin Bouchard"
  excerpt: "You can download my CV here"
last_modified_at: 2023-09-14
---

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2901.0357656017773!2d-1.5664932920959476!3d43.35536246618617!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x20c9a7db7942bae5!2sInstitut%20National%20de%20Recherche%20Agronomique%20INRA!5e0!3m2!1sfr!2sfr!4v1664890223406!5m2!1sfr!2sfr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>


Contact
: <i class="fas fa-briefcase"></i> Scientific projet manager [<i class="fas fa-globe"></i>](https://www.scimabio-interface.fr)
: <i class="fas fa-briefcase"></i> SCIMABIO Interface, science-management interface for biodiversity conservation
: <i class="fas fa-map-pin"></i> 173 route de Saint-Jean de Luz, Saint-Pée sur Nivelle, 64310, France
: <i class="fas fa-envelope-open-text"></i> [colin.bouchard@scimabio-interface.fr](mailto:colin.bouchard@scimabio-interface.fr)

------------------------------------------------------------------------
<figure style="width: 400px" class="align-center">

<img src="{{ site.baseurl }}/documents/Scimabio final-01.png"/>

</figure>
------------------------------------------------------------------------
