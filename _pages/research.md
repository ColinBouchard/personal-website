---
title: "Research interests"
layout: splash
author_profile: true
classes:
    - landing
    - dark-theme
permalink: /research/
header:
  overlay_color: "#000"
  overlay_filter: "0.1"
  overlay_image: photos_images/IMG_1773.jpg
  caption: "Photo credit: ©Colin Bouchard"
excerpt: "Portfolio of some of my current and past projects"
intro: 
  - excerpt: 'Individual variability in space use and the resulting effects on population dynamics and sexual selection constitutes the guideline of my current research project.'
feature_row:
  - image_path: documents/Fig5_P_Plot_Grid_RecruitmentStockBest_50m_RecruitmentPtau.jpg
    image_caption: "Photo credit: ©Colin Bouchard"
    alt: "aggregation and recruitment"
    title: "Spatial aggregation effects on population dynamics"
    excerpt: "In a wide range of taxa, population dynamics exhibit yearly variations. Temporal variations in stock (e.g., number of eggs), density-dependent processes (e.g., density-dependent compensation)..."
    url: "/research_SRAggreg/"
    btn_label: "Read More"
    btn_class: "btn--primary"
  - image_path: documents/Scheme_encounter.jpg
    image_caption: "Credit: ©Colin Bouchard"
    alt: "sexual network from spatially explicit data"
    title: "Inferring sexual network from spatially explicit data"
    excerpt: "Spatial distribution and movements of potential breeders may influence their possibility to mate. In systems where sexual reproduction occurs, the fertilisation of gametes with low mobility (e.g., internal fertilisation, immobile eggs, or eggs settling to the bottom) requires <i>i</i>) an encounter between potential sexual partners and that <i>ii</i>) these two partners access each other..."
    url: "/research_Network/"
    btn_label: "Read More"
    btn_class: "btn--primary"
  - image_path: photos_images/ReleaseRadio_LowQuality.gif
    image_caption: "Credit: ©Colin Bouchard"
    title: "How sexual maturity affects movements"
    excerpt: "Atlantic salmon exhibit alternative reproductive tactic with individuals maturing in rivers. Those individuals, namely mature parr, are much smaller than anadromous individuals who mature at sea. The size difference may affect their space use tactic to find females..."
    url: "/research_SpaceUseParr/"
    btn_label: "Read More"
    btn_class: "btn--primary"
feature_row2:
  - image_path: photos_images/IMG_4887.jpg
    image_caption: "Photo credit: ©Colin Bouchard"
    alt: "extreme mortality events"
    title: "Investigate extreme mortality events in small cetaceans"
    excerpt: "Spatial distribution of cetaceans may lead to strong interactions between human activities (e.g., fisheries, maritime transport, ...) and cetaceans ..."
    url: "/research_EMECetacean/"
    btn_label: "Read More"
    btn_class: "btn--primary"
feature_row3:
  - image_path: documents/Scheme_PEContext.jpg
    image_caption: "Credit: ©Colin Bouchard"
    alt: "spatial synchrony"
    title: "Spatial synchrony between Atlantic salmon populations"
    excerpt: 'Understanding the drivers of ecological stability is mandatory to predict the response of organisms to environmental changes. The portfolio effect (hereafter PE) describes how diversity between and within populations can increase temporal stability of the complex of populations'
    url: "/research_SpatialSynchrony/"
    btn_label: "Read More"
    btn_class: "btn--primary"
row_Camargue:
  - image_path: photos_images/P_Photo_Vaccares.jpeg
    image_caption: "Credit: ©Colin Bouchard"
    alt: "glass eel migration"
    title: "Glass eel migration and recruitment within a complex Mediterranean hydro-system"
    excerpt: 'Characterizing the recruitment and upstream migration of glass eels within a complex Mediterranean hydro-system to identify spatio-temporal dynamics, as well as environmental and barrier management conditions associated with high levels of recruitment.'
    url: "/research_GlassEelCamargue/"
    btn_label: "Read More"
    btn_class: "btn--primary"
last_modified_at: 2022-10-04
row_Diazepam:
  - image_path: photos_images/GlassEel_Contamination/P_Photo_Chronotron_IMG_9705_Low.jpeg
    image_caption: "Credit: ©Colin Bouchard"
    alt: "glass eel migratory behavior"
    title: "Behavioural alterations of glass eels' migratory behaviour by a psychiatric drug"
    excerpt: 'Assessing behavioural alterations caused by an anxiolytic on the migratory behaviour of glass eels. We tested whether individuals contaminated with diazepam were less active and exhibited a different migratory behavior than uncontaminated individuals. We also assessed whether a contamination by an anxiolytic drug impacted the boldness-exploratory syndrome of individuals.'
    url: "/research_GlassEelDiazepam/"
    btn_label: "Read More"
    btn_class: "btn--primary"
last_modified_at: 2022-10-23
---

{% include feature_row id="intro" type="center" %}

{% include feature_row id="row_Camargue" type="left" %}

{% include feature_row id="row_Diazepam" type="right" %}

{% include feature_row %}

{% include feature_row id="feature_row2" type="left" %}

{% include feature_row id="feature_row3" type="right" %}
