---
title: "How sexual maturity affects movements"
layout: splash
author_profile: true
classes:
    - landing
    - dark-theme
permalink: /research_SpaceUseParr/
header:
  overlay_color: "#000"
  overlay_filter: "0.1"
  overlay_image: photos_images/DSC_2375-GLISE-INRA_Extract.jpg
  caption: "©S. Glise / INRA"
excerpt: "   "
last_modified_at: 2022-10-04
---

---
<figure style="width: 400px" class="align-left">
  <img src="{{ site.baseurl }}/photos_images/ReleaseRadio_LowQuality.gif" alt="">
  <figcaption> One-year old salmon parr tagged with a radio-tag. ©Colin Bouchard.</figcaption>
</figure> 


<p align="justify">
Atlantic salmon exhibit <span style="font-weight:bold">alternative reproductive tactic</span> with individuals maturing in rivers. Those individuals, namely mature parr, are much smaller than anadromous individuals who mature at sea. The size difference may affect their <span style="font-weight:bold">space use tactic</span> to find females. Mature parr might opt for a staying behaviour rather than a cruising one because of their smaller body size and their limited resources. Nevertheless, they should also leave their natal habitats (i.e., foraging habitats of salmon parr) to go to breeding ones to maximise their encounter probability with females. In contrast, immature should stay in their natal habitats to feed. Although space use of anadromous throughout the breeding season is well studied, space use of mature parr is rarely investigated <i>in natura</i> and especially during a breeding season.
</p>


<figure style="width: 400px" class="align-right">
  <img src="{{ site.baseurl }}/photos_images/P_Photo_Lurberria_GOPR0120_Low.jpeg" alt="">
  <figcaption> Electrofishing to catch Atlantic salmon parr. ©Colin Bouchard.</figcaption>
</figure> 


<p align="justify">
We monitored space use of 30 mature one-year-old parr and ten immature throughout a complete breeding season in the Nivelle population (France). Daily monitoring data enabled us to characterise space use tactics of mature parr and inter-individual variability. Comparing with space use of immature individuals highlighted that <span style="font-weight:bold">sexual maturity</span> was associated with <span style="font-weight:bold">longer distances covered</span> and <span style="font-weight:bold">broader home ranges</span>. Mature individuals also exhibited higher inter-individuals variability suggesting that maturity may favour a diversity of space use tactics in parr like in anadromous individuals.
</p>


---
 - **Bouchard C.**, Lange F., Rives J., and C. Tentelier. **2020**.  Sexual maturity increases mobility and heterogeneity in individual space use in Atlantic salmon (*Salmo salar*) parr. *Journal of Fish Biology*, **96** (4): 925-938. [link](https://onlinelibrary.wiley.com/doi/abs/10.1111/jfb.14282)  
 - **Bouchard, C.** 2018. In Atlantic salmon, space use of potential breeders stabilises population dynamics and shapes sexual selection (Doctoral dissertation, Pau). [link](https://www.researchgate.net/publication/331563783_In_Atlantic_salmon_space_use_of_potential_breeders_stabilises_population_dynamics_and_shapes_sexual_selection)

---

<figure style="width: 400px" class="align-center">
  <img src="{{ site.baseurl }}/photos_images/P_Photo_TabletRiver_GOPR0091_Low.jpeg" alt="">
  <figcaption> Saving GPS position of each individual caught to release them at their catching position. ©Colin Bouchard.</figcaption>
</figure> 

