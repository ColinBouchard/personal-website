---
title: "Publications"
layout: splash
last_modified_at: 2022-10-04
author_profile: true
classes:
    - landing
    - dark-theme
permalink: /publications/
header:
  overlay_color: "#000"
  overlay_filter: "0.1"
  overlay_image: photos_images/P_Photo_RiverMouth_IMG_1469_Low.jpeg
  actions:
    - label: "Let's go!"
      url: "https://scholar.google.com/citations?user=DuQ5OKIAAAAJ&hl=fr&oi=ao"
  caption: "©Colin Bouchard"
excerpt: "You can access my Google Scholar profile for an actualised list of my publications"
toc: true
toc_label: "Contents"
toc_icon: "align-justify"
last_modified_at: 2022-10-04
---


# Published
- **Bouchard C.**, Drouineau H., Lambert P., Boutron O., and D. Nicolas. **2022**.  Spatio-temporal variations in glass eel recruitment at the entrance pathways of a Mediterranean delta. *ICES journal*, **79** (6): 1874-1887. [link](https://academic.oup.com/icesjms/article/79/6/1874/6633760)  
- Vallecillo D., Guillemain M., Authier M., **Bouchard C.**, Cohez D., Vialet E., Massez G., Vandewalle P. and J. Campagnon. **2022**.  Accounting for detection probability with overestimation by integrating double monitoring programs over 40 years. *Plos One*, **17** (3): 17. [link](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0265730)
- **Bouchard C.**, Buoro M., Lebot C., and S. M. Carlson. **2021**.  Synchrony in population dynamics of juvenile Atlantic salmon: analyzing spatio-temporal variation and the influence of river flow and demography. *Canadian Journal of Fisheries and Aquatic Sciences*, **79** (5): 782-794. [link](https://doi.org/10.1139/cjfas-2021-0017)
- **Bouchard C.**, Lange F., Rives J., and C. Tentelier. **2020**.  Sexual maturity increases mobility and heterogeneity in individual space use in Atlantic salmon (*Salmo salar*) parr. *Journal of Fish Biology*, **96** (4): 925-938. [link](https://onlinelibrary.wiley.com/doi/abs/10.1111/jfb.14282)  
- **Bouchard C.**, Bracken C.,  Dabin W., Van Canneyt O., Ridoux V., Spitz J., and M. Authier. **2019**. A risk-based indicator for marine mammal conservation: strandings and probabilistic forecast of extreme mortality events. *Conservation Letters*, **12** (4): 10. [link](https://doi.org/10.1111/conl.12639)  
- **Bouchard C.**, Bracken C.,  Dabin W., Van Canneyt O., Ridoux V., Spitz J., and M. Authier. **2019**. Data from: A risk-based indicator for marine mammal conservation: strandings and probabilistic forecast of extreme mortality events. *Data Dryad* [link](https://doi.org/10.5061/dryad.vj7sh73)
- Tentelier C., **Bouchard C.**, Bernardin A., Tauzin A., Aymes J.C., and J. Rives. The dynamics of spawning acts by a semelparous fish and its associated energetic expenses. *BioArXiv*, **preprint**. [link](https://doi.org/10.1101/436295)
- **Bouchard C.**, Bardonnet A.,  Buoro M., Prévost E., and C. Tentelier. **2018**. Effects of spatial aggregation of nests on population recruitment: the case of a small population of Atlantic salmon. *Ecosphere*, **9** (4). [link](https://doi.org/10.1002/ecs2.2178)
- Bacles C.F.E., **Bouchard C.**,  Lange F., Manicki A., Tentelier C., and O. Lepais. **2018**. Estimating the effective number of breeders from single parr samples for conservation monitoring of wild populations of Atlantic salmon (*Salmo salar*). *Journal of Fish Biology*, **92**: 699-726. [link](http://onlinelibrary.wiley.com/doi/10.1111/jfb.13537/abstract)
- Giroux H., **Bouchard C.**, and M. Britten. **2014**. Combined effect of renneting pH, cooking temperature, and dry salting on the contraction kinetics of rennet-induced milk gels. *International Dairy Journal*, **35** (1): 70-74. [link](http://www.sciencedirect.com/science/article/pii/S0958694613002677)

---

# In review
 - Daupagne L., Bouchard C., Michaud A., Dhamenlincourt M., Lasnes E., and C.Tentelier. Considering temporal variations in mating opportunities: consequences on sexual selection estimates. *Animal Behaviour*, **In Review**
 - Vallecillo D., Guillemain M., **Bouchard C.**, Roques S., and J. Campagnon. Changes of water management in the functional unit for the conservation of waterfowl: a case study of Teal *Anas creccam*. *Biodiversity and Conservation*, **In Review**
 - **Bouchard C.**, Boutron O., Lambremon J., Drouineau H., Lambert P., and D. Nicolas. Impacts of environmental conditions and management of sluice gates on glass eel migration. *Coastal and Shelf Science journal*, **In Review** 2nd round

---

# In preparation
 - **Bouchard C.**, Tentelier C., Sebihi S., Monperrus M., and V. Bolliet. Behavioural alterations of migratory behaviour by a psychiatric drug. **In Prep**
 - **Bouchard C.**, Boutron O., and D. Nicolas. Estimation of the migration speed of glass eels during the colonization of a lagoon hydro-system. **In Prep**
 - **Bouchard C.**, Boutron O., and D. Nicolas. Dynamics and characterization of the glass eel recruitment within a newly available habitat and relationships with some environmental drivers of the upstream migration behaviour. **In Prep**
 - **Bouchard C.**, Grant M.C., Tentelier. C, and T. Pizzari. Evaluating the local social structure and its effects on the pre-copulatory selection acting on Atlantic salmon males by inferring the population sexual network. **In Prep**

---

# Reports
- **Bouchard C.**, Boutron O., and D. Nicolas. **2022**. Synthèse et analyse de la continuité écologique à l'échelle du delta de Camargue : caractérisation du recrutement et de la dynamique migratoire des civelles d'anguilles Européennes dans le delta de Camargue. *Rapport final action MA 12 - contrat de Delta Camargue - Phase 2* : 156. [link](https://colinbouchard.gitlab.io/personal-website/documents/Bouchardetal_RapportFinal_AERM.pdf)  
- Van Canneyt O., **Bouchard C.**,  Dabin W., Demaret F. and G. Dorémus. **2012**. Les échouages de mammifères marins sur le littoral français en 2012. *Rapport UMS - Observatoire PELAGIS pour le Ministère de l’Ecologie, du Développement Durable et de l’Energie, Direction de l’eau et de la biodiversité, Programme Observatoire du Patrimoine Naturel* : 51. [link]({http://crmm.univ-lr.fr/images/pdf/Rapport2012.pdf)  
