---
title: "Inferring sexual network from spatially explicit data"
layout: splash
author_profile: true
classes:
    - landing
    - dark-theme
permalink: /research_Network/
header:
  overlay_color: "#000"
  overlay_filter: "0.1"
  overlay_image: photos_images/Network_Scheme.jpg
  caption: "©Colin Bouchard"
excerpt: "Evaluating the local social structure and its effects on the pre-copulatory selection acting on Atlantic salmon males by inferring the population sexual network"
last_modified_at: 2019-10-21
---

---
<figure style="width: 400px" class="align-left">
  <img src="{{ site.baseurl }}/documents/Scheme_encounter.jpg" alt="">
  <figcaption> Scheme of impacts of space use on encounter and social environment. ©Colin Bouchard.</figcaption>
</figure> 


<p align="justify">
<span style="font-weight:bold">Spatial distribution and movements</span> of potential breeders may influence their possibility to mate. In systems where sexual reproduction occurs, the fertilisation of gametes with low mobility (e.g., internal fertilisation, immobile eggs, or eggs settling to the bottom) requires <i>i</i>) an <span style="font-weight:bold">encounter</span> between potential sexual partners and that <i>ii</i>) these two partners access each other (Jones 2009, Kokko & Rankin 2006, Kuijper et al. 2012). Sexual selection arises when the reproductive success varies among phenotypes or the phenotypic trait value, which is inheritable (Kuijper et al. 2012). Such selection acts, thus, on the processes influencing the encounters and accesses potential mates, namely the <span style="font-weight:bold">pre-copulatory competition</span>. For instance, several males may compete to have access to the same female as in Birds-of-paradise (Kirkpatrick & Ryan 1991). The pre-copulatory competition involves either direct interactions (Hunt et al. 2009, Jones 2009, Kokko & Rankin 2006, Kuijper et al. 2012) or indirect ones through the mate choice of individuals of the other sex (Andersson & Simmons 2006, Hunt et al. 2009, Kokko & Rankin 2006). In polygynous species, sexual selection may also realise through <span style="font-weight:bold">post-copulatory competition</span>, which happened when several individuals mate with the same individual (Kuijper et al. 2012). All of these processes — mate choice, direct interactions, post-copulatory competition — usually have a density-dependent component and, above all, are <span style="font-weight:bold">relative</span> (Gasparini et al. 2013, Kokko & Rankin 2006).
</p>


<figure style="width: 400px" class="align-right">
  <img src="{{ site.baseurl }}/documents/Grid_Plot_MatrixNetwork_Encounter09-1.pdf" alt="">
  <figcaption> Bipartite networks of males (red) and females (blue) based on their encounter probability (a) during one breeding season. In network plots, probabilities lower than 0.05 are not displayed. ©Colin Bouchard.</figcaption>
</figure> 


<p align="justify">
The spatial distribution of potential breeders is one among factors affecting the Operational Sex Ratio (Shuster & Wade 2003), the sex ratio of individuals participating in the reproduction (i.e., potential breeders). Intuitively, a local skewed OSR experienced by individuals can modify the competition that they suffer from accessing potential mates (Chuard et al. 2016, Weir et al. 2011). The <span style="font-weight:bold">social environment</span>, which is impacting by the spatial distribution of potential breeders, may affect the individual number of mates and the mating success of individuals (i.e., number of matings producing juveniles) by influencing mate choices, OSR, and mate monopolisation. In this way, the social environment may change the intra-sexual competition (Procter et al. 2012). When social selection or multilevel selection occurs, the reproductive success of a focal individual is affected by its phenotype but also by the phenotype of its social competitors (McDonald et al. 2013, Muniz et al. 2015). This study highlighted the importance of accounting for the <span style="font-weight:bold">local social structure</span> in natura when investigating sexual selection and pre-copulatory competition.
</p>


---
 - **Bouchard C.**, Grant M.C., Tentelier. C, and T. Pizzari. Evaluating the local social structure and its effects on the pre-copulatory selection acting on Atlantic salmon males by inferring the population sexual network.. **In Prep**
 - **Bouchard, C.** 2018. In Atlantic salmon, space use of potential breeders stabilises population dynamics and shapes sexual selection (Doctoral dissertation, Pau). [link](https://www.researchgate.net/publication/331563783_In_Atlantic_salmon_space_use_of_potential_breeders_stabilises_population_dynamics_and_shapes_sexual_selection)

---

<figure style="width: 400px" class="align-center">
  <img src="{{ site.baseurl }}/photos_images/P_Photo_FemSalmon_INRA-GLISE_Low.jpg" alt="">
  <figcaption> Female of Atlantic salmon ready to spawn. ©S. Glise / INRA.</figcaption>
</figure> 
