---
title: "CV — Resume"
layout: splash
author_profile: true
classes:
    - landing
    - dark-theme
permalink: /resume/
header:
  overlay_color: "#000"
  overlay_filter: "0.1"
  overlay_image: documents/photo_header_cv.jpg
  actions:
    - label: "Download"
      url: "documents/CV_BouchardColin.pdf"
  caption: "©Colin Bouchard"
excerpt: "You can download my CV here"
last_modified_at: 2022-10-04
---


<figure class="align-left">
  <img src="{{site.baseurl}}/photos_images/IMG_2685.jpeg" alt="" />
  <figcaption>Photo credit: © Colin Bouchard.</figcaption>
</figure>

# Education
<i class="fas fa-graduation-cap"></i> PhD Ecology Evolution
: December 2018
: "In Atlantic salmon, space use of potential breeders stabilises population dynamics and shapes sexual selection."
: University of Pau & Pays de l'Adour
: Thesis director: Dr Tentelier Cédric <i class="fas fa-envelope-open-text"></i> [cedric.tentelier@univ-pau.fr](mailto:cedric.tentelier@univ-pau.fr)

<i class="fas fa-graduation-cap"></i> MSc Biology Ecology Evolution
: 2015
: University of Toulouse III (Paul Sabatier)
: "Modeling and probabilistic prediction of Extreme Mortality Events : the case of marine mammals"
: Thesis director: Dr Authier Matthieu <i class="fas fa-envelope-open-text"></i> [matthieu.authier@univ-lr.fr](mailto:matthieu.authier@univ-lr.fr)
